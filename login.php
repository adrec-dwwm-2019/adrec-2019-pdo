<?php
require_once 'functions.php';
getHeader('Connexion');

if($_POST && !empty($_POST)){
    if(!empty($_POST['login']) && !empty($_POST['password'])) {
        connectUser($_POST['login'], $_POST['password']);
    } else {
        redirect('/login.php?error=field-empty');
    }
}

// echo generatePassword('theau') . '<br>';
// echo generatePassword('marcel');

?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <h1 class="mt-3 mb-5 text-center">Se connecter</h1>
            <form method="post">
                <label for="login">Login</label>
                <input type="text" class="form-control mb-3" name="login" id="login" required>

                <label for="password">Password</label>
                <input type="password" class="form-control mb-3" name="password" id="password" required>

                <button type="submit" class="btn btn-primary">
                    Se connecter
                </button>
            </form>
        </div>
    </div>
</div>

<?php
getFooter();