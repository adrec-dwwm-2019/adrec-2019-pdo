<?php

session_start();

// A patcher avec la page sign in
//redirectToLogin();

function getHeader(string $title = 'Adrec PDO')
{
    include 'partials/header.php';
}


function getFooter()
{
    include 'partials/footer.php';
}


function isLoggedIn(): bool
{
   
    if (isset($_SESSION['loggedIn'])) {
        return true;
    } 
    return false;
}

//Redirect à la paàge login quand l'utilisateur n'est pas connecté
function redirectToLogin(): void
{
   
    if(strpos($_SERVER['REQUEST_URI'], 'login.php') != 1) {
        if(!isLoggedIn()) {
            echo 'je redirect';
            redirect('/login.php');
        }
    }
}



function redirect(string $path): void
{
    header('Location: ' . $path);
}


function dump($var) {
    echo '<pre>' . var_export($var, true) . '</pre>';
}

function initDb() {
    
    try {
        $db = new PDO('mysql:host=localhost;dbname=adrec_pdo;charset=utf8', 'root', '');
    } catch (Exception $e) {
        echo $e->getMessage() . $e->getFile() . ' L' . $e->getLine();
    }

    return $db;
}

function connectUser(string $email, string $password)
{
    $db = initDb();

    $request = $db->prepare("SELECT * FROM `users` WHERE email = ?");

    $request->execute([$email]);
    
    $user = $request->fetch(PDO::FETCH_OBJ);

    if ($user == false) {
        redirect('/login.php?auth=fail');
    }

    if(password_verify($password, $user->password)) {
        $_SESSION['loggedIn'] = 'toto';
        redirect('/');
    } else {
        redirect('/login.php?auth=fail');
    }
    
}


function generatePassword(string $password): string
{
    return password_hash($password, PASSWORD_DEFAULT);
}


function getVideoGame()
{
    $db = initDb();

    $sql = "SELECT `nom`, `console`, `prix`, `commentaires` FROM `jeux_video` WHERE prix < :price AND console = :console";

    $request = $db->prepare($sql);

    $params = [
        'price' => 40,
        'console' => 'PC',
    ];

    $request->execute($params);

    $games = $request->fetchAll(PDO::FETCH_OBJ);

    return $games;
}


function makeQuery($SQL)
{
    $db = initDb();

    $request = $db->query($SQL);

    $result = $request->fetchAll(PDO::FETCH_OBJ);

    return $result;
}


function regirsterUser($params){
    $db = initDb();
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = "INSERT INTO `users` (`email`, `password`, `pseudo`) VALUES (:fmail, :fpassword, :fpseudo);";

    

    $params = [
        'fmail' => $params['login'],
        'fpassword' => $params['password'],
        'fpseudo' => $params['pseudo'],
    ];

    try {
        $request = $db->prepare($sql);
        $request->execute($params);
    } catch (Exception $e) {

        if($e->getCode() == 23000) {
            //redirect('/sign-in.php?error=duplicate-email');
        }
        
    }

    

}