<?php
require_once 'functions.php';
getHeader('Accueil PDO');

$games = getVideoGame();
?>

<h1>
    Jeux vidéos 
</h1>




<table class="table">
  <thead>
    <tr>
      <th scope="col">Nom</th>
      <th scope="col">Prix</th>
      <th scope="col">Plateforme</th>
      <th scope="col">Avis</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($games as $game): ?>
        <div>
            <tr>
                <th scope="row">
                    <?php echo $game->nom; ?>
                </th>
                <td>
                    <?php echo $game->prix; ?>€
                </td>
                <td>
                    <?php echo $game->console; ?>
                </td>
                <td>
                    <?php echo $game->commentaires; ?>
                </td>
            </tr>
            
        </div>
    <?php endforeach?>
    

   
  </tbody>
</table>



<?php
getFooter();


